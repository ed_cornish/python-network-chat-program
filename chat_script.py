#-------------------------------------------------------------------------------
# Name:        chat_script
# Purpose:     Windows based program to send text from a console session on one PC to another
#              on the same network - tested over wired LAN
#
# Author:      Ed Cornish
#
# Created:     21/10/2015
# Copyright:   Copyright (c) 2015, 2016 Edward Cornish
# Licence:     

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES #OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#-------------------------------------------------------------------------------

#!/usr/bin/python

""" This program implements a simple way of sending messages between machines
    on the same network from within a windows command prompt.
    The program uses the sockets module for network communication and the
    msvcrt module for command prompt manipulation.

    Other modules used:
        select
        errno
        time
        sys

    TODO:
        - Deletion of entered characters reflected in accumulated command string
        - Notify when remote host disconnects
        - More comprehensive error handling
        - Saving contacts (probably as JSON or YAML)
        - Multiple conversations/participants
        - GUI

"""

#----------- import modules
import msvcrt, time, sys, errno, socket, select

#----------- define string constants
EMPTY_STRING = ''
QUIT_STRING = 'goodbye'
CONNECT_STRING = 'hello'
HELP_STRING = 'help'

#----------- define help strings in a dictionary
helpdict = {
    CONNECT_STRING : CONNECT_STRING + ' <hostname> - will attempt to connect to an instance of this \nprogram running on the remote machine <hostname>',
    QUIT_STRING : QUIT_STRING + ' - will close any open connections and exit the program',
    HELP_STRING : HELP_STRING + ' - will print this commands listing'
}

#----------- constants used by sockets
PORT = 12345
RECV_BUFF_SIZE = 1024

#----------- class to handle user input
class CmdPrompt():
    """
    This class runs an MS command prompt and accumulates entered characters
    """
    def __init__(self):
        """ Init the string to empty """
        self.curr_str = EMPTY_STRING

    def Run(self):
        """ If a key has been hit in the console, either add it to the accumulated
            string or return it as a string to send as a command to the rest of
            the program """
        send_str = EMPTY_STRING

        # Check for console keypresses
        if msvcrt.kbhit():
            # Store the captured char
            kb_char = msvcrt.getch()
            # In the case of Enter being pressed, output the accumulated string
            # as a command
            if kb_char == '\r':
                sys.stdout.write('%60s\n' % self.curr_str)
                sys.stdout.flush()
                send_str = self.curr_str
                self.curr_str = EMPTY_STRING
            # Accumulate the entered character
            else:
                self.curr_str += kb_char
        else:
            # Display a prompt
            sys.stdout.write("-> %-s  \r" % self.curr_str )
            sys.stdout.flush()

        # Return any comnmand string to send to the host script for handling
        return send_str

#----------- class to manage sockets
class SockMgr():
    """ This class handles the initialisation of a server socket and any
        client sockets required for connections to other hosts """

    def __init__(self):
        """ Set up the server socket and start it listening. Initialise data
            needed for socket selection """
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((socket.gethostname(), PORT))
        self.server_socket.listen(5)
        print 'Listening on port %d' % PORT
        # Init socket lists
        self.recv_list = [self.server_socket]
        self.send_list = []
        self.error_list = []

    def Connect(self, target):
        """ Set up a connection to a target host. If the connection was refused,
            clean up and notify the user """
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            # Attempt the connection
            print 'Trying to connect...'
            self.client_socket.connect((target, PORT))

        except socket.error as serr:
            # Check if the connection was refused by the target host
            if serr.errno == errno.ECONNREFUSED:
                print 'Connection refused by', target
                self.client_socket.close()
            else:
                # Re-raise the exception as it is not a refused connection...
                raise serr

        print 'Connected to', target
        # Add the new client socket to the send and read lists
        self.send_list.append(self.client_socket)
        self.recv_list.append(self.client_socket)


    def Run(self, send_str):
        """ Use select to handle any sockets that have data to receive or can be sent to"""
        if CONNECT_STRING in send_str:
            command_str = send_str.split(' ')
            target_host = command_str[1]
            self.Connect(target_host)

        readable, writable, errored = select.select(self.recv_list, self.send_list, self.error_list, 0)
        
        # Loop over sockets that can be sent to and send any string passed
        for s in writable:
            if send_str != EMPTY_STRING:
                s.send(send_str)
                
        # Loop over readable sockets
        for s in readable:
            if s is self.server_socket:
                # New connection detected
                self.new_socket, address = self.server_socket.accept()
                self.recv_list.append(self.new_socket)
                self.send_list.append(self.new_socket)
                print "Connection from", address
            else:
                # Handle incoming data (print to console)
                data = s.recv(RECV_BUFF_SIZE)
                if data:
                    print('++ '+data)
                    if data == QUIT_STRING:
                        send_str = QUIT_STRING
                        break


    def Quit(self):
        """ Close all open sockets """
        for s in self.recv_list:
            s.close()
        for s in self.send_list:
            s.close()

#----------- Print out help info for user
def PrintHelp():
    print 'Help listing:'
    for entry in helpdict.keys():
        sys.stdout.write('%-60s\n\n' % helpdict[entry])
    sys.stdout.flush()

#----------- create instances of classes
prompt = CmdPrompt()
sock_manager = SockMgr()

#----------- enter main program loop
while True:
    quit_sent = False
    send_str = EMPTY_STRING
    # Avoid polling the input like a maniac
    time.sleep(0.01)

    # Run the input processing
    send_str = prompt.Run()

    # Handle commands which are not socket specific
    if send_str == QUIT_STRING:
        quit_sent = True
    elif send_str == HELP_STRING:
        PrintHelp()

    # Run the socket processing
    sock_manager.Run(send_str)

    if quit_sent:
        # Destroy everything
        sock_manager.Quit()
        exit()

#----------- end of Source