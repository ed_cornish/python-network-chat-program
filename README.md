# README #

### What is this repository for? ###

* This is my attempt to write a *simple* (for now) IM\chat program that will communicate between two hosts on a network
* The script uses the sockets module and the windows specific msvcrt to parse console input

### NOTES ###

* This script uses modules built into the standard Python distribution, no extra dependencies should be required.
* The script uses a hardcoded port number which is not blocked by the network used in testing. YMMV.
* Upon starting the script, typing 'help' at the prompt will display the list of commands (currently 3!)
* The script is **single ended**; an instance of the script *must* be running on each end of the connection before you try and connect :)

### TO DO ###
* A comprehensive to-do list is in the code comments but some of the big issues are:
    * Deleting characters from the prompt does not work...
    * Not many exceptions are well-handled at present...
    * Saving contacts for ease of reconnection
    * A GUI!
    * A way to carry more than one conversation on at once